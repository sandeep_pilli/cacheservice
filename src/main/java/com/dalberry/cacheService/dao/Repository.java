package com.dalberry.cacheService.dao;

public interface Repository {
	void save(String key, String value);
	String get(String key);
	void update(String key, String value);
	void delete(String key);
	String getAll(String key);
}
