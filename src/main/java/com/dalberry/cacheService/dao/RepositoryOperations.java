package com.dalberry.cacheService.dao;

public interface RepositoryOperations {

	void save(String key, String value, int expireTime);

	String fetch(String key);

	boolean expire(String key, int timeInSeconds);

	Long getTTL(String key);

	void delete(String key);

}
