package com.dalberry.cacheService.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/cache")
public class MiiContoller {


	@Autowired
	@Qualifier("redisTemplate")
	RedisTemplate<String, Object> redisTemplate;

	@RequestMapping(value = "/mii/getvalue", method = RequestMethod.POST)
	public @ResponseBody String getValue(@RequestBody String key) {
		System.out.println("mii controller :: " + key);

		redisTemplate.opsForValue().set("chandu","chandooo");
		return redisTemplate.opsForValue().get("chandu").toString();
	}
}
