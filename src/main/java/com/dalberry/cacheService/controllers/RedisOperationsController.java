package com.dalberry.cacheService.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/cache")
public class RedisOperationsController {
	
	@RequestMapping(value = "/notification", method = RequestMethod.POST)
	public @ResponseBody Object notification(@RequestBody Object request) {
		System.out.println("NOTIFICATION IS :: " + request.toString());
		return null;
	}


}
