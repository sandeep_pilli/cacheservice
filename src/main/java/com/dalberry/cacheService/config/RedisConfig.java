package com.dalberry.cacheService.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

import redis.clients.jedis.JedisPoolConfig;

@Configuration
@PropertySource(value = {"classpath:application.properties"})
@EnableConfigurationProperties
public class RedisConfig {

	@Value("${redis.usePool}")
	boolean usePool;
	@Value("${redis.maxTotal}")
	int maxTotal;
	@Value("${redis.maxWaitMillis}")
	int maxWaitMillis;
	@Value("${redis.testOnBorrow}")
	boolean testOnBorrow;
	@Value("${redis.testOnReturn}")
	boolean testOnReturn;
	@Value("${redis.testWhileIdle}")
	boolean testWhileIdle;
	@Value("${redis.minEvictableIdleTimeMillis}")
	int minEvictableIdleTimeMillis;
	@Value("${redis.timeBetweenEvictionRunsMillis}")
	int timeBetweenEvictionRunsMillis;
	@Value("${redis.numTestsPerEvictionRun}")
	int numTestsPerEvictionRun;
	@Value("${redis.hostName}")
	String redisHost;
	@Value("${redis.port}")
	int redisPort;
	@Value("${redis.password}")
	String redisPassword;
	@Value("${redis.timeout}")
	int timeout;

	@Bean
	JedisPoolConfig jedisPoolConfig(){
		JedisPoolConfig poolConfig = new JedisPoolConfig();
		poolConfig.setMaxTotal(maxTotal);
		poolConfig.setMaxWaitMillis(maxWaitMillis);
		poolConfig.setTestOnBorrow(testOnBorrow);
		poolConfig.setTestWhileIdle(testWhileIdle);
		poolConfig.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis);
		poolConfig.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis);
		poolConfig.setNumTestsPerEvictionRun(numTestsPerEvictionRun);
		return poolConfig;
	}

	@Bean
	JedisConnectionFactory jedisConnectionFactory(JedisPoolConfig jedisPoolConfig) {
		JedisConnectionFactory jedisConFactory = new JedisConnectionFactory();
		jedisConFactory.setHostName(redisHost);
		jedisConFactory.setPort(redisPort);
		jedisConFactory.setPassword(redisPassword);
		jedisConFactory.setTimeout(timeout);
		jedisConFactory.setUsePool(usePool);
		jedisConFactory.setPoolConfig(jedisPoolConfig);
		return jedisConFactory;
	}

	@Bean
	public RedisTemplate<String, Object> redisTemplate(JedisConnectionFactory jedisConnectionFactory) {
		RedisTemplate<String, Object> template = new RedisTemplate<String, Object>();
		template.setConnectionFactory(jedisConnectionFactory);
		return template;
	}
}